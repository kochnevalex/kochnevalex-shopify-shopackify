// class TimeFormatted extends HTMLElement {

//   connectedCallback() {
//     let date = new Date(this.getAttribute('datetime') || Date.now());

//     this.innerHTML = new Intl.DateTimeFormat("default", {
//       year: this.getAttribute('year') || undefined,
//       month: this.getAttribute('month') || undefined,
//       day: this.getAttribute('day') || undefined,
//       hour: this.getAttribute('hour') || undefined,
//       minute: this.getAttribute('minute') || undefined,
//       second: this.getAttribute('second') || undefined,
//       timeZoneName: this.getAttribute('time-zone-name') || undefined,
//     }).format(date);
//   }
// };

// customElements.define("time-formatted", TimeFormatted);

// class QuantityField extends HTMLElement {
//   constructor() {
//     super();
//     this.input = this.querySelector('input');
//     this.changeEvent = new Event('change', { bubbles: true }) 
//     this.querySelectorAll('button').forEach(
//       (button) => button.addEventListener('click',
//         this.onButtonClick.bind(this)
//       )
//     );
//   }

//   onButtonClick(event) {
//     event.preventDefault();
//     const previousValue = this.input.value;
//     event.target.name === 'plus' ? this.input.stepUp() : this.input.stepDown();

//     if (previousValue !== this.input.value) this.input.dispatchEvent(this.changeEvent);
//   }
// }
// customElements.define('quantity-field', QuantityField);

customElements.define('cart-', class TimeFormatted extends HTMLElement {

})

if (!customElements.get('product-form')) {
  customElements.define('product-form', class TimeFormatted extends HTMLElement {
    constructor() {
      super();
      this.form = this.querySelector('form');
      this.form.querySelector('[name=id]').disabled = false;
      this.form.addEventListener('submit', this.onFormSubmit.bind(this));
    }

    onFormSubmit(evt) {
      evt.preventDefault();
      const config = fetchConfig('javascript');
      config.headers['X-Requested-With'] = 'XMLHttpRequest';
      config.body = JSON.stringify(JSON.parse(serializeForm(this.form)));
      fetch(`${routes.cart_add_url}`, config)
        .then((response) => response.json())
        .then((response) => {
          console.log(response);
          if (response.status) {
            // this.handleErrorMessage(response.description);
            console.log(response);
            return;
          }

          // this.cartNotification.renderContents(response);
        })
        .catch((e) => {
          console.error(e);
        })
      // .finally(() => {
      //   submitButton.classList.remove('loading');
      //   submitButton.removeAttribute('aria-disabled');
      //   this.querySelector('.loading-overlay__spinner').classList.add('hidden');
      // });
    }
  })
}
